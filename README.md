SPW - SECURE PASSWORDS
======================

[NAME](#name)
  [INSTALLATION](#installation)
  [SYNOPSIS](#synopsis)
  [VERSION](#version)
  [DESCRIPTION](#description)
  [OPTIONS](#options)
  [EXAMPLES](#examples)
  [CONFIG FILES](#config-files)
  [CONFIG OPTIONS](#config-options)
  [AUTHOR](#author)
  [SEE ALSO](#see-also)

* * * * *

NAME
----

spw − generate secure passwords using multiple algorithms

INSTALLATION
------------

**Arch Linux**

The **spw** is available in AUR:

```bash
[ yaourt | pacaur ] -S spw
```

SYNOPSIS
--------

**spw** [ *OPTION* ] [ *pw\_length* ] [ *num\_pw* ]

VERSION
-------

This man page documents **spw** in version 1.1.

DESCRIPTION
-----------

The **spw** program generates secure passwords using multiple algorithms. The program allows for the use of hardware Random Number Generators aiming to provide the highest possible level of security and entropy of one’s password.
 The script has been tested with TrueRNG (http://ubld.it/products/truerng-hardware-random-number-generator) and OneRNG (http://onerng.info).

**NOTE:** The **spw** can generate various strengths of passwords so be aware where one uses them. In particular do not use pseudo-readable passwords in places where the password could be attacked via off-line brute-force attack.

The **spw** program is designed to be used both interactively, and in shell scripts. Hence, its default behavior differs depending on whether the standard output is a tty device or a pipe to another program. Used interactively, **spw** will display a screenful of passwords, allowing the user to pick a single password, and then quickly erase the screen. This prevents someone from being able to "shoulder surf" the user’s chosen password.

When standard output (stdout) is not a tty, **spw** will only generate one password, as this tends to be much more convenient for shell scripts, and in order to be compatible with previous versions of this program.

OPTIONS
-------

**No OPTION specified.**

When no **-r, -x, --sha1 or --sha256** specified, the **spw** generates secure, non-dictionary based passwords. The passwords generated using this algorithm are considered the most secure, however they are not easy for a user to remember.

User can influence the behaviour of the algorithm by passing one or more of the options: [-u | -l | -a | -d | -s | -e | -b]. This leads to generate less or more secure passwords.

In order to check the quality of the generated password, please add option **-E** to calculate its entropy.

**−r, −-readable**

This option changes the behaviour of the **spw,** so it uses the dictionary file to generate pseudo-readable passwords.
 The **spw** will create passwords from randomised atoms generated from words in a dictionary. User can use a dictionary with words in any language. The file should be UTF-8 encoded and consists of words listed one per line.

**−x, −-xkcd**

The **spw** will generate password phrases, as per an idea presented in XKCD comic stripe: *https://xkcd.com/936.* The pass-phrases are considered very secure and extremely easy to memorise, as they are constructed from words in language(s) familiar to the user. The dictionary file should be quite large, so the generated pass-phrases will carry high entropy.
 While generating pass-phrases, the *pw\_length* will be interpreted as a number of words from a dictionary instead of number of characters. (Max 10 words, unless option **−-overwrite** is specified.)
 User can use a dictionary with words in any language. The file should be UTF-8 encoded and consists of words listed one per line.

**−-sha1** */path/to/file[\#seed]*

The **spw** will generate a list of sha-1 hashes of given file and the optional seed to create passwords. It will allow user to derive the same passwords later.

**WARNING:** The passwords generated using this option is considered not very random. If one uses this option, make sure the attacker can not obtain a copy of the file.

**NOTE:** However, this option is perfect to generate a list of OTP (One-Time-Passwords).

**−-sha256** */path/to/file[\#seed]*

The **spw** will generate a list if sha-256 hashes of given file and the optional seed to create passwords. It will allow user to derivate the same passwords later.

**WARNING:** The passwords generated using this option carries more entropy, but is still considered not very random. If one uses this option, make sure the attacker can not obtain a copy of the file.

**NOTE:** However, this option is perfect to generate a list of OTP (One-Time-Passwords).

**−s, −-special**

Include special characters in the passwords.
 Special characters are: !@\#\$%&\*-+=

**−e, −-extra**

Include extra characters in the passwords.
 Extra characters are: \^?\_:;.,\~|

**−b, −-brackets**

Include brackets characters in the passwords.
 Brackets characters are: ()[]{}\<\>

**-m, --no-meta**

Don't include shell meta characters.

**−d, −-no-digits**

Don’t include numbers in the generated passwords.

**−u, −-no-upper**

Don’t include any capital letters in the generated passwords.

**−l, --no-lower**

Don’t include lower case characters in passwords.

**−a, --no-ambiguous**

Don’t use characters that could be confused by the user when printed, such as ’l’ and ’1’, or ’0’ or ’O’. This reduces the number of possible passwords significantly, and as such reduces the quality of the passwords. In general use of this option is not recommended.

**−1, −-single**

Print generated passwords in a single column.

**−h, --help**

Print a help message.

Advanced options. **
 −E, −-entropy**

Calculate password’s entropy. This is very useful to evaluate the quality of the generated passwords. Higher entropy value means stronger password which can sustain the brute-force attack longer.

**−-source** *RNGDEV*

User can specify an entropy source. This option is the heart of **spw** secure password generator. It allows to use a hardware RNG in order to generate the most secure and most trusted password.
 Default: /dev/urandom

**−-dictionary** *DICT*

A dictionary file to be used with dictionary based algorithms ( **−r** or **−x** ). User can use any dictionary with words in any language. The file should be UTP-8 encoded and consists of words listed one per line.
 Default: /usr/share/dict/spw\_en

XKCD specific options. **
 −-min** *MIN\_WORD\_LENGTH*

Specify a minimum word length for password phrase. This option is used in combination with XKCD.

**−-max** *MAX\_WORD\_LENGTH*

Specify a maximum word length for password phrase. This option is used in combination with XKCD.

**−-separator** *SEPARATOR*

User can specify a words separator for generated pass-phrases.
 Default is a dash (-).

**−S, −-shannon**

This switch modify entropy calculation for XKCD password phrases. Instead of using a number of words in the pass-phrase, the total number of characters in generated pass-phrase will be used to calculate entropy. This will produce a much higher value. Use with caution as it can lead to a false sense of password quality.

**−O, −-overwrite**

User can overwrite limits. By default, the pass-phrases are limited to max 10 words phrases. If for any reason one needs longer pass-phrases, the limit can be elevated.

**-v, --version**

Show version.

EXAMPLES
--------

**spw**

Passing no parameters will call the script with default values. It will generate 40 secure passwords (23 characters long) and print them in double column.

**spw 10**

Will generate 40 secure passwords, 10 characters long each.

**spw 10 20**

Will generate 20 secure passwords, 10 characters long each.

**spw -seb**

Will generate 40 of the most secure passwords, 23 characters long each, consisting of special characters, extra characters and brackets.

**spw -lu 20**

Will generate 40 passwords, 20 digits long each.

**spw -udE**

Will generate 40 passwords, 23 lower case characters only, and calculate entropy of each password.

**spw --source** */dev/TrueRNG* **50 10**

Will generate 10 passwords, 50 characters long each, using hardware TrueRNG device as a source of entropy.

**spw -r**

Will generate 40 pseudo-readable passwords, 23 characters long each.

**spw -rdE**

Will generate 40 pseudo-readable passwords, 23 characters long each without digits, and calculate entropy of each password.

**spw -x**

Will generate 40 pass-phrases, 10 words each.

**spw -x --min 3 --max 5 --separator ’\*’ 5 10**

Will generate 10 pass-phrases, 5 words each, from words between 3 and 5 characters, and separate words with an asterisk.

**spw -xE 7**

Will generate 40 pass-phrases, 7 words each, and calculate entropy (based on number of randomised words and total number of words in the dictionary).

**spw -xS 5**

Will generate 40 pass-phrases, 5 words each, and calculate entropy (based on number of characters in each pass-phrase).

**spw --sha1** *\~/Music/Master\_of\_Puppets.mp3\#my-user@gmail.com*

Will generate a list of 40 passwords based on a mp3 file, salted with user’s gmail address. This can be used as a passwords for gmail. In case user forgets the password, it can be re-generated using the same file and the same salt.

**spw --sha256** *\~/Music/nebula.png\#MasterPassword* **25**

Will generate a list of 25 passwords based on a png file, salted with user’s MasterPassword. This can be used as a OTP for further use, as it can be re-generated using the same file and the same MasterPassword.

CONFIG FILES
------------

**Global config**

The spw honors a system-wide/global config file which will be looked for first at

/etc/spw.conf

and then

**User config**

which should be located at

$HOME/.spw.conf

**Note:**  The *user config* will always take precedence over the *system-wide config*.

CONFIG OPTIONS
--------------

**Format:**

The config options are defined as [ key = value ] pairs.

Boolean values are defined as: [ True | Yes | 1 ] or [ False | No | 0 ]

**num_pw = 40**

Generate "num_pw" passwords

**pw_length = 23**

Generate "pw_length" long passwords

**noupper = False**

Do not use upper-case characters

**nolower = False**

Do not use lower-case characters

**nodigits = False**

Do not use digits

**noambiguous = False**

Do not use ambiguous characters

**special = False**

Use special characters

**extra = False**

Use extra characters

**brackets = False**

Use brackets

**single = False**

Display passwords in single column

**source = /dev/urandom**

Use "source" as the entropy source

**dictionary = /usr/share/dict/spw_en**

Use "dictionary" file for readable passwords

AUTHOR
------

The **spw** was written by **Plague Doctor**. Based on the program *pwgen* written by Theodore Ts. The *spw* further develops the basic idea of password security.

SEE ALSO
--------

**passwd**(1), **pwgen**(1)

* * * * *

## Donate

If you like this project, please donate. You can send coins to the following addresses.

**Bitcoin**: 3KWsKEw3Ewu7vcTREjQUuR8LUf4QXoZEHK

**Litecoin**: MHaqGAqMoQGiTkNyg7w8haC6mU25Frgi3M
