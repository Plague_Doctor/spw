.\" Standard preamble:
.\" ========================================================================
.de Sp \" Vertical space (when we can't use .PP)
.if t .sp .5v
.if n .sp
..
.\" ========================================================================
.\"
.TH SPW 1 "May 2017" "spw version 1.6"
.SH NAME
The
.B spw
\- generate secure passwords using multiple algorithms
.SH SYNOPSIS
.B spw
[
.I OPTION
]
[
.I pw_length
]
[
.I num_pw
]
.SH VERSION
This man page documents
.B spw
in version 1.1.
.SH DESCRIPTION
The
.B spw
program generates secure passwords using multiple algorithms.
The program allows for the use of hardware Random Number Generators
aiming to provide the highest possible level of security and entropy
of one's password.
.br
The script has been tested with
TrueRNG (http://ubld.it/products/truerng-hardware-random-number-generator)
and OneRNG (http://onerng.info).
.PP
.B NOTE:
The
.B spw
can generate various strengths of passwords so be aware where one uses them.
In particular do not use pseudo-readable passwords in places where
the password could be attacked via off-line brute-force attack.
.PP
The
.B spw
program is designed to be used both interactively, and in shell scripts.
Hence, its default behavior differs depending on whether the standard output
is a tty device or a pipe to another program.  Used interactively,
.B spw
will display a screenful of passwords, allowing the user to pick a single
password, and then quickly erase the screen.  This prevents someone from
being able to "shoulder surf" the user's chosen password.
.PP
When standard output (stdout) is not a tty,
.B spw
will only generate one password, as this tends to be much more convenient
for shell scripts, and in order to be
compatible with previous versions of this program.
.B
.SH OPTIONS
.TP
.B No OPTION specified.
When no
.B -r, -x, --sha1 or --sha256
specified, the
.B spw
generates secure, non-dictionary based passwords. The passwords generated
using this algorithm are considered the most secure, however they are not easy
for a user to remember.
.IP
User can influence the behaviour of the algorithm by passing one or more of the options:
[-u | -l | -a | -d | -s | -e | -b | -m]. This leads to generate less or more secure passwords.
.IP
In order to check the quality of the generated password, please add option
.B -E
to calculate its entropy.
.TP
.B \-r, \--readable
This option changes the behaviour of the
.B spw,
so it uses the dictionary file to generate pseudo-readable passwords.
.br
The
.B spw
will create passwords from randomised atoms generated from words in
a dictionary. User can use a dictionary with words in any language.
The file should be UTF-8 encoded and consists of words listed one per line.
.TP
.B \-x, \--xkcd
The
.B spw
will generate password phrases, as per an idea presented in XKCD comic stripe:
.I https://xkcd.com/936.
The pass-phrases are considered very secure and extremely easy to memorise,
as they are constructed from words in language(s) familiar to the user.
The dictionary file should be quite large, so the generated pass-phrases will carry
high entropy.
.br
While generating pass-phrases, the
.I pw_length
will be interpreted as a number of words from a dictionary instead of number of characters.
(Max 10 words, unless option
.B \--overwrite
is specified.)
.br
User can use a dictionary with words in any language. The file should be UTF-8 encoded
and consists of words listed one per line.
.TP
.B \--sha1 \fI/path/to/file[#seed]
The
.B spw
will generate a list of sha-1 hashes of given file and the optional seed to create passwords.
It will allow user to derive the same passwords later.
.IP
.B WARNING:
The passwords generated using this option is considered not very random.
If one uses this option, make sure the attacker can not obtain a copy of the file.
.IP
.B NOTE:
However, this option is perfect to generate a list of OTP (One-Time-Passwords).
.TP
.B \--sha256 \fI/path/to/file[#seed]
The
.B spw
will generate a list if sha-256 hashes of given file and the optional seed to create passwords.
It will allow user to derivate the same passwords later.
.IP
.B WARNING:
The passwords generated using this option carries more entropy, but is still considered
not very random. If one uses this option, make sure the attacker can not obtain a copy
of the file.
.IP
.B NOTE:
However, this option is perfect to generate a list of OTP (One-Time-Passwords).
.TP
.B \-s, \--special
Include special characters in the passwords.
.br
Special characters are: !@#$%&*-+=
.TP
.B \-e, \--extra
Include extra characters in the passwords.
.br
Extra characters are: ^?_:;.,~|
.TP
.B \-b, \--brackets
Include brackets characters in the passwords.
.br
Brackets characters are: ()[]{}<>
.TP
.B \-m, \--no-meta
Don't include shell meta characters.
.br
Shell meta characters are: ><*?[]`$|&()#!;\\"
.TP
.B \-d, \--no-digits
Don't include numbers in the generated passwords.
.TP
.B \-u, \--no-upper
Don't include any capital letters in the generated passwords.
.TP
.B \-l, --no-lower
Don't include lower case characters in passwords.
.TP
.B \-a, --no-ambiguous
Don't use characters that could be confused by the user when printed,
such as 'l' and '1', or '0' or 'O'.  This reduces the number of possible
passwords significantly, and as such reduces the quality of the
passwords. In general use of this option is not recommended.
.TP
.B \-1, \--single
Print generated passwords in a single column.
.TP
.B \-h, --help
Print a help message.
.PP
Advanced options.
.TP
.B \-E, \--entropy
Calculate password's entropy. This is very useful to evaluate the quality
of the generated passwords. Higher entropy value means stronger password
which can sustain the brute-force attack longer.
.TP
.B \--source \fIRNGDEV
User can specify an entropy source. This option is the heart of
.B spw
secure password generator. It allows to use a hardware RNG in order to
generate the most secure and most trusted password.
.br
Default: /dev/urandom
.TP
.B \--dictionary \fIDICT
A dictionary file to be used with dictionary based algorithms (
.B \-r
or
.B \-x
). User can use any dictionary with words in
any language. The file should be UTP-8 encoded and consists of words
listed one per line.
.br
Default: /usr/share/dict/spw_en
.PP
XKCD specific options.
.TP
.B \--min \fIMIN_WORD_LENGTH
Specify a minimum word length for password phrase. This option is used
in combination with XKCD.
.TP
.B \--max \fIMAX_WORD_LENGTH
Specify a maximum word length for password phrase. This option is used
in combination with XKCD.
.TP
.B \--separator \fISEPARATOR
User can specify a words separator for generated pass-phrases.
.br
Default is a dash (-).
.TP
.B \-S, \--shannon
This switch modify entropy calculation for XKCD password phrases.
Instead of using a number of words in the pass-phrase, the total number
of characters in generated pass-phrase will be used to calculate entropy.
This will produce a much higher value. Use with caution as it can lead to  a false sense of password quality.
.TP
.B \-O, \--overwrite
User can overwrite limits. By default, the pass-phrases are limited to max 10
words phrases. If for any reason one needs longer pass-phrases, the limit
can be elevated.
.TP
.B \-v, \--version
Show version.
.SH EXAMPLES
.TP
.B spw
Passing no parameters will call the script with default values. It will generate
40 secure passwords (23 characters long) and print them in double column.
.TP
.B spw 10
Will generate 40 secure passwords, 10 characters long each.
.TP
.B spw 10 20
Will generate 20 secure passwords, 10 characters long each.
.TP
.B spw -seb
Will generate 40 of the most secure passwords, 23 characters long each, consisting of
special characters, extra characters and brackets.
.TP
.B spw -lu 20
Will generate 40 passwords, 20 digits long each.
.TP
.B spw -udE
Will generate 40 passwords, 23 lower case characters only, and calculate entropy
of each password.
.TP
.B spw --source \fI/dev/TrueRNG \fB50 10
Will generate 10 passwords, 50 characters long each, using hardware TrueRNG device
as a source of entropy.
.TP
.B spw -r
Will generate 40 pseudo-readable passwords, 23 characters long each.
.TP
.B spw -rdE
Will generate 40 pseudo-readable passwords, 23 characters long each without digits,
and calculate entropy of each password.
.TP
.B spw -x
Will generate 40 pass-phrases, 10 words each.
.TP
.B spw -x --min 3 --max 5 --separator '*' 5 10
Will generate 10 pass-phrases, 5 words each, from words between 3 and 5 characters,
and separate words with an asterisk.
.TP
.B spw -xE 7
Will generate 40 pass-phrases, 7 words each, and calculate entropy (based on number
of randomised words and total number of words in the dictionary).
.TP
.B spw -xS 5
Will generate 40 pass-phrases, 5 words each, and calculate entropy (based on number
of characters in each pass-phrase).
.TP
.B spw --sha1 \fI~/Music/Master_of_Puppets.mp3#my-user@gmail.com
Will generate a list of 40 passwords based on a mp3 file, salted with user's gmail address.
This can be used as a passwords for gmail. In case user forgets the password, it can be
re-generated using the same file and the same salt.
.TP
.B spw --sha256 \fI~/Music/nebula.png#MasterPassword \fB25
Will generate a list of 25 passwords based on a png file, salted with user's MasterPassword.
This can be used as a OTP for further use, as it can be re-generated using the same file
and the same MasterPassword.

.SH CONFIG FILES
.TP
.B Global config
The
.B spw
honors a system-wide/global config file which will be looked for first at
.Sp
\&/etc/spw.conf
.Sp
and then
.TP
.B User config
which should be located at
.Sp
\&$HOME/.spw.conf
.TP
.B Note:
The user config will always take precedence over the system-wide config.

.SH CONFIG OPTIONS
.TP
.B Format:
The
.I config options
are defined as [ key = value ] pairs.
.br
.I Boolean values
are defined as: [ True | Yes | 1 ] or [ False | No | 0 ]
.TP
.B num_pw = <40>
Generate "num_pw" passwords
.TP
.B pw_length = <23>
Generate "pw_length" long passwords
.TP
.B noupper = <False>
Do not use upper-case characters
.TP
.B nolower = <False>
Do not use lower-case characters
.TP
.B nodigits = <False>
Do not use digits
.TP
.B noambiguous = <False>
Do not use ambiguous characters
.TP
.B special = <False>
Use special characters
.TP
.B extra = <False>
Use extra characters
.TP
.B brackets = <False>
Use brackets
.TP
.B single = <False>
Display passwords in single column
.TP
.B source = </dev/urandom>
Use "source" as the entropy source
.TP
.B dictionary = </usr/share/dict/spw_en>
Use "dictionary" file for readable passwords

.SH AUTHOR
The
.B spw
was written by
.B Plague Doctor
<plague at privacyrequired dot com>.
Based on the program
.I pwgen
written by Theodore Ts. The
.I spw
further develops the basic idea of password security.
.SH SEE ALSO
.BR passwd (1), pwgen (1)
